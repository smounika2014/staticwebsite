import React from "react";
import Navbar from "../Components/Navbar";
import Hero from "../Components/Hero";
import Info from "../Components/Info";
import About from "../Components/About";
import Case from "../Components/Case";
import Reviews from "../Components/Reviews";
import Team from "../Components/Team";
import Footer from "../Components/Footer";
import It from "../Components/It";
import Hardware from "../Components/Hardware"
import Ourclients from "../Components/Ourclients";
//import Slider  from "../Components/Slider";

function Home() {
  return (
    <div className="home-section">
      <Navbar />
      <Hero />
      <Info />
      <About />
      <Case />
      <It/>
      <Hardware/>
      <Reviews />
      
      <Team />
      <Ourclients/>
    
      <Footer />
    </div>
  );
}

export default Home;

import React from "react";
import Team from "../Assets/doctor-group.png";
//import SolutionStep from "./SolutionStep";
import "../Styles/About.css";

function About() {
  return (
    <div className="about-section" id="about">
      <div className="about-image-content">
        <img src={Team} alt="Team" className="about-image1" />
      </div>

      <div className="about-text-content">
        <h3 className="about-title">
          <span>Empowering Digital Transformation</span>
        </h3>
        <p className="about-description">
        At GT Global Solutions, we are passionate professionals committed to transforming
businesses through cutting-edge IT solutions. Explore our journey, mission, and
values.
Description : Empowering Digital Transformation. At GT Global Solutions, we are passionate
professionals committed to transforming businesses through cutting-edge IT solutions. Our journey is
marked by a dedication to innovation, a clear mission to empower organizations digitally, and
unwavering values that guide our approach to client partnerships
        </p>

      

        
      </div>
    </div>
  );
}

export default About;

import React from "react";
import InformationCard from "./InformationCard";
//import { faHeartPulse, faTruckMedical, faTooth } from "@fortawesome/free-solid-svg-icons";
import "../Styles/Info.css";

function Info() {
  return (
    <div className="info-section" id="services">
      <div className="info-title-content">
        <h3 className="info-title">
          <span>Comprehensive IT Solutions</span>
        </h3>
        <p className="info-description">
        Explore our bespoke software development, IT consulting, and scalable solutions
crafted to meet your unique business needs.
Description : Comprehensive IT Solutions. Explore our bespoke software development, IT consulting,
and scalable solutions crafted to meet your unique business needs. From conceptualization to
implementation, our services are designed to enhance efficiency, boost productivity, and catalyze
growth for your organization.

        </p>
      </div>

      <div className="info-cards-content">
        <InformationCard
          title="company news through our blog"
          description="Our team of experts shares valuable information, tips, and thought leadership to keep you ahead of the curve in the fast-paced world of technology.
          "
        
        />

        <InformationCard
          title="Stay updated with industry trends"
          description="Stay updated with industry trends, tech insights, and company news through our blog."
          
        />

        <InformationCard
          title="tech insights"
          description="Our team of experts shares valuable information, tips, and thought leadership to keep you ahead of the curve in the fast-paced world of technology.
          "
          
        />
      </div>
    </div>
  );
}

export default Info;

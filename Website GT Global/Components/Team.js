import React from "react";
import Card from "./Card";
import profile1 from "../Assets/profile-1.png";
import profile2 from "../Assets/profile-2.png";
import profile3 from "../Assets/profile-3.png";
import profile4 from "../Assets/profile-4.png";
import "../Styles/Team.css";

function Team() {
  return (
    <div className="doctor-section" id="Team">
      <div className="dt-title-content">
        <h3 className="dt-title">
          <span>Join Our Team</span>
        </h3>

        <p className="dt-description">
        Explore career opportunities at GT Global Solutions and be part of driving
innovation. Description : Join Our Team. Explore career opportunities at GT Global Solutions and be part of
driving innovation. We are always on the lookout for talented individuals who share our passion for
technology and dedication to client success. Join us in shaping the future of IT solutions
        </p>
      </div>

      <div className="dt-cards-content">
        <Card
          img={profile1}
          name="Kranthi k "
          title="CEO"
          //stars="4.9"
         // reviews="1800"
        />
        <Card
          img={profile2}
          name="satya priya "
          title="content creator"
        //  stars="4.8"
         // reviews="700"
        />
        <Card
          img={profile3}
          name="Prasanna"
          title="Developer"
        //  stars="4.7"
         // reviews="450"
        />
        <Card
          img={profile4}
          name="Vamsi"
          title="Architect"
         // stars="4.8"
         // reviews="500"
        />
      </div>
    </div>
  );
}

export default Team;

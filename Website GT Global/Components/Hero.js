import React, { useEffect, useState } from "react";
import Team from "../Assets/doctor-picture.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarCheck, faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { useNavigate  } from "react-router-dom";
import "../Styles/Hero.css";

function Hero() {
  const navigate = useNavigate();
  const [goUp, setGoUp] = useState(false);

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  const handleBookAppointmentClick = () => {
    navigate("/appointment");
  };

  useEffect(() => {
    const onPageScroll = () => {
      if (window.scrollY > 600) {
        setGoUp(true);
      } else {
        setGoUp(false);
      }
    };
    window.addEventListener("scroll", onPageScroll);

    return () => {
      window.removeEventListener("scroll", onPageScroll);
    };
  }, []);

  return (
    <div className="section-container">
      <div className="hero-section">
        <div className="text-section">
          <p className="text-headline">We give you solutions</p>
          <h2 className="text-title">
            Find your Solutions
          </h2>
          <p className="text-descritpion">
          Welcome to GT Global Solutions, where innovation meets expertise. Elevate your business
with our customized IT solutions designed to drive success in the digital era.
Description: Welcome to GT Global Solutions, where innovation meets expertise. Elevate your
business with our customized IT solutions designed to drive success in the digital era. Whether you're
a startup seeking efficient software solutions or an established enterprise aiming for digital
transformation, GT Global Solutions is your strategic partner in navigating the evolving technological
landscape.

          </p>
          
          <div className="text-stats">
            <div className="text-stats-container">
             
            </div>

            <div className="text-stats-container">
              
            </div>

            <div className="text-stats-container">
              
            </div>
          </div>
        </div>

        <div className="hero-image-section">
          <img className="hero-image1" src={Team} alt="Doctor" />
        </div>
      </div>

      <div
        onClick={scrollToTop}
        className={`scroll-up ${goUp ? "show-scroll" : ""}`}
      >
        <FontAwesomeIcon icon={faAngleUp} />
      </div>
    </div>
  );
}

export default Hero;

import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import "../Styles/LegalDocs.css";
import { GrTechnology } from "react-icons/gr";

function LegalDocs() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  });

  return (
    <div className="legal-section-title">
      <h1 className="legal-siteTitle">
        <Link to="/">
          GT GLOBAL SOLUTIONS <span className="legal-siteSign"></span><GrTechnology />
        </Link>
      </h1>

      <div className="legal-text-content">
        <p className="legal-title">General Info</p>
        <p className="legal-description">
          Welcome to Gt Global ,Our mission is to provide Solutions to your problems 
        </p>

        <p className="legal-title">Privacy Policy:
</p>
        <p className="legal-description">
        Protecting Your Privacy

        Understand how we handle and protect your data in our comprehensive privacy
policy.
Description : Protecting Your Privacy. Understand how we handle and protect your data in our
comprehensive privacy policy. Your privacy is important to us, and we are committed to maintaining
the confidentiality and security of the information you entrust to us.
        </p>

        <p className="legal-title">Detailed terms of service for website users.
</p>
        <p className="legal-description">
        Detailed terms of service for website users.
        Description : Detailed terms of service for website users. Familiarize yourself with the terms of
service that govern your use of our website. This section provides clarity on user responsibilities,
usage policies, and the terms under which our services are provided.

        </p>

        

        
      </div>

      <div className="legal-footer">
        <p>© GT GLOBAL </p>
      </div>
    </div>
  );
}

export default LegalDocs;

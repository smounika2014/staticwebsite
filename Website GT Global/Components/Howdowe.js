import React from "react";
import Team from "../Assets/doctor-book-appointment.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleCheck,
  faCalendarCheck,
} from "@fortawesome/free-solid-svg-icons";
import { useNavigate  } from "react-router-dom";
import "../Styles/BookAppointment.css";

function Howdowe() {
  const navigate = useNavigate();

  const handleBookAppointmentClick = () => {
    navigate("/appointment");
  };

  return (
    <div className="ba-section">
      <div className="ba-image-content">
        <img src={Team} alt="Doctor Group" className="ba-image1" />
      </div>

      <div className="ba-text-content">
        <h3 className="ba-title">
          <span>Case Studies</span>
        </h3>
        <p className="ba-description">
        Real-world Impact
Explore success stories showcasing GT Global Solutions' impact on clients.
Description : Real-world Impact. Explore success stories showcasing GT Global Solutions' impact on clients. Dive into concrete examples of how our solutions have overcome challenges, improved operations, and contributed to the success of businesses across different domains.

        </p>

        
      

        
      </div>
    </div>
  );
}

export default Howdowe;
